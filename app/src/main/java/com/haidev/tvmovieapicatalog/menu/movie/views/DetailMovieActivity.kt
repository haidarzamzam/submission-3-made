package com.haidev.tvmovieapicatalog.menu.movie.views

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.haidev.tvmovieapicatalog.R
import com.haidev.tvmovieapicatalog.menu.movie.models.MovieModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupData()
    }

    private fun setupData() {
        if (intent.extras != null) {
            val listMovieModel: MovieModel.ResultsMovie = intent.getParcelableExtra(EXTRA_DATA_LIST)
            Picasso.get().load("https://image.tmdb.org/t/p/w185" + listMovieModel.posterPath).into(ivMovie)
            txtTitleMovie.text = listMovieModel.title
            txtDateMovie.text = listMovieModel.releaseDate
            txtDescMovie.text = listMovieModel.overview

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
