package com.haidev.tvmovieapicatalog.menu.movie.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.tvmovieapicatalog.menu.movie.models.MovieModel
import com.haidev.tvmovieapicatalog.network.repositories.MovieRepository

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listMovieResponse: MutableLiveData<MovieModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private var mainRepository = MovieRepository()

    fun requestListMovie() {
        isLoading.set(true)
        mainRepository.getListMovie({
            isLoading.set(false)
            listMovieResponse.postValue(it)
        }, {
            isLoading.set(false)
        })
    }

    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}