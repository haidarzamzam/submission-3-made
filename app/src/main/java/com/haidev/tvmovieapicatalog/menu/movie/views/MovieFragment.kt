package com.haidev.tvmovieapicatalog.menu.movie.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmovieapicatalog.R
import com.haidev.tvmovieapicatalog.databinding.FragmentMovieBinding
import com.haidev.tvmovieapicatalog.menu.movie.adapters.ItemMovieAdapter
import com.haidev.tvmovieapicatalog.menu.movie.models.MovieModel
import com.haidev.tvmovieapicatalog.menu.movie.viewmodels.MovieViewModel
import kotlinx.android.synthetic.main.fragment_movie.*

class MovieFragment : Fragment() {

    private lateinit var movieBinding: FragmentMovieBinding
    private lateinit var vmMovie: MovieViewModel

    private lateinit var adapter: ItemMovieAdapter
    private var listMovie: MutableList<MovieModel.ResultsMovie> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        movieBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie, container, false)
        vmMovie = ViewModelProviders.of(this).get(MovieViewModel::class.java)
        movieBinding.movie = vmMovie

        vmMovie.listMovieResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmMovie.error.observe(this, Observer {
            handlingError(it)
        })

        setupSwipeRefresh()
        setupRecycler()
        vmMovie.requestListMovie()
        return movieBinding.root
    }

    private fun setupSwipeRefresh() {
        movieBinding.swipeRefreshLayoutMovie.setOnRefreshListener {
            movieBinding.swipeRefreshLayoutMovie.isRefreshing = false
            vmMovie.requestListMovie()
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        movieBinding.rvListMovie.layoutManager = lManager
        movieBinding.rvListMovie.setHasFixedSize(true)

        adapter = ItemMovieAdapter(context!!, listMovie)
        movieBinding.rvListMovie.adapter = adapter
    }

    private fun onListDataChange(movieModel: MovieModel?) {
        listMovie.clear()
        listMovie.addAll(movieModel?.resultsMovie!!)
        rvListMovie.post {
            adapter.notifyDataSetChanged()
        }
        movieBinding.progressBarMovie.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {

        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
        movieBinding.progressBarMovie.isVisible = false
    }

}
