package com.haidev.tvmovieapicatalog.menu.tv.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.haidev.tvmovieapicatalog.R
import com.haidev.tvmovieapicatalog.databinding.FragmentTvBinding
import com.haidev.tvmovieapicatalog.menu.tv.adapters.ItemTvAdapter
import com.haidev.tvmovieapicatalog.menu.tv.models.TvModel
import com.haidev.tvmovieapicatalog.menu.tv.viewmodels.TvViewModel
import kotlinx.android.synthetic.main.fragment_tv.*

class TvFragment : Fragment() {

    private lateinit var tvBinding: FragmentTvBinding
    private lateinit var vmTv: TvViewModel

    private lateinit var adapter: ItemTvAdapter
    private var listTv: MutableList<TvModel.ResultTv> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        tvBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tv, container, false)
        vmTv = ViewModelProviders.of(this).get(TvViewModel::class.java)
        tvBinding.tv = vmTv

        vmTv.listTvResponse.observe(this, Observer {
            onListDataChange(it)
        })
        vmTv.error.observe(this, Observer {
            handlingError(it)
        })

        setupSwipeRefresh()
        setupRecycler()
        vmTv.requestListTv()
        return tvBinding.root
    }

    private fun setupSwipeRefresh() {
        tvBinding.swipeRefreshLayoutTv.setOnRefreshListener {
            tvBinding.swipeRefreshLayoutTv.isRefreshing = false
            vmTv.requestListTv()
        }
    }

    private fun setupRecycler() {
        val lManager = LinearLayoutManager(context)
        tvBinding.rvListTv.layoutManager = lManager
        tvBinding.rvListTv.setHasFixedSize(true)

        adapter = ItemTvAdapter(context!!, listTv)
        tvBinding.rvListTv.adapter = adapter
    }

    private fun onListDataChange(modelTv: TvModel?) {
        listTv.clear()
        listTv.addAll(modelTv?.resultsTv!!)
        rvListTv.post {
            adapter.notifyDataSetChanged()
        }
        tvBinding.progressBarTv.isVisible = false
    }

    private fun handlingError(throwable: Throwable?) {
        tvBinding.progressBarTv.isVisible = false
        Toast.makeText(context, throwable?.message, Toast.LENGTH_SHORT).show()
    }

}
