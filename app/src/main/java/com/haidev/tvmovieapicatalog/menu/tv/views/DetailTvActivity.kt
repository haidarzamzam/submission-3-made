package com.haidev.tvmovieapicatalog.menu.tv.views

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.haidev.tvmovieapicatalog.R
import com.haidev.tvmovieapicatalog.menu.tv.models.TvModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_tv.*

class DetailTvActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA_LIST = "extra_data_list"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tv)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupData()
    }

    private fun setupData() {
        if (intent.extras != null) {
            val listTvModel: TvModel.ResultTv = intent.getParcelableExtra(EXTRA_DATA_LIST)
            Picasso.get().load("https://image.tmdb.org/t/p/w185" + listTvModel.posterPath).into(ivTv)
            txtTitleTv.text = listTvModel.name
            txtDateTv.text = listTvModel.firstAirDate
            txtDescTv.text = listTvModel.overview

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
