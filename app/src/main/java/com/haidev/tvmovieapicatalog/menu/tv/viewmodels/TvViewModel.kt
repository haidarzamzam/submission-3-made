package com.haidev.tvmovieapicatalog.menu.tv.viewmodels

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.haidev.tvmovieapicatalog.menu.tv.models.TvModel
import com.haidev.tvmovieapicatalog.network.repositories.TvRepository

class TvViewModel(application: Application) : AndroidViewModel(application) {
    var isLoading: ObservableField<Boolean> = ObservableField()
    var listTvResponse: MutableLiveData<TvModel> = MutableLiveData()
    var error: MutableLiveData<Throwable> = MutableLiveData()

    private var mainRepository = TvRepository()

    fun requestListTv() {
        isLoading.set(true)
        mainRepository.getListTv({
            isLoading.set(false)
            listTvResponse.postValue(it)
        }, {
            isLoading.set(false)
        })
    }

    override fun onCleared() {
        super.onCleared()
        mainRepository.cleared()
    }
}
