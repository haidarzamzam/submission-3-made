package com.haidev.tvmovieapicatalog.menu.movie.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class MovieModel(
    @SerializedName("page") @Expose val page: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") @Expose val totalResults: Int,
    @SerializedName("results") @Expose var resultsMovie: MutableList<ResultsMovie>

) : Parcelable {

    @Parcelize
    data class ResultsMovie(
        @SerializedName("adult") @Expose val adult: Boolean,
        @SerializedName("genre_ids") @Expose val genreIds: List<Int>,
        @SerializedName("id") @Expose val id: Int,
        @SerializedName("original_language") @Expose val originalLanguage: String,
        @SerializedName("original_title") @Expose val originalTitle: String,
        @SerializedName("overview") @Expose val overview: String,
        @SerializedName("popularity") @Expose val popularity: Double,
        @SerializedName("poster_path") @Expose val posterPath: String,
        @SerializedName("release_date") @Expose val releaseDate: String,
        @SerializedName("title") @Expose val title: String,
        @SerializedName("video") @Expose val video: Boolean,
        @SerializedName("vote_average") @Expose val voteAverage: Double,
        @SerializedName("vote_count") @Expose val voteCount: Int
    ) : Parcelable

}