package com.haidev.tvmovieapicatalog.menu.tv.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TvModel(
    @SerializedName("page") @Expose val page: Int,
    @SerializedName("total_pages") @Expose val totalPages: Int,
    @SerializedName("total_results") @Expose var totalResults: Int,
    @SerializedName("results") @Expose val resultsTv: MutableList<ResultTv>
) : Parcelable {

    @Parcelize
    data class ResultTv(
        @SerializedName("backdrop_path") @Expose val backdropPath: String,
        @SerializedName("first_air_date") @Expose val firstAirDate: String,
        @SerializedName("genre_ids") @Expose val genreIds: List<Int>,
        @SerializedName("id") @Expose val id: Int,
        @SerializedName("name") @Expose val name: String,
        @SerializedName("origin_country") @Expose val originCountry: List<String>,
        @SerializedName("original_language") @Expose val originalLanguage: String,
        @SerializedName("original_name") @Expose val originalName: String,
        @SerializedName("overview") @Expose val overview: String,
        @SerializedName("popularity") @Expose val popularity: Double,
        @SerializedName("poster_path") @Expose val posterPath: String,
        @SerializedName("vote_average") @Expose val voteAverage: Double,
        @SerializedName("vote_count") @Expose val voteCount: Int
    ) : Parcelable
}