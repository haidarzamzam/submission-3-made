package com.haidev.tvmovieapicatalog.network

import com.haidev.tvmovieapicatalog.menu.movie.models.MovieModel
import com.haidev.tvmovieapicatalog.menu.tv.models.TvModel
import io.reactivex.Observable
import retrofit2.http.GET


interface RestApi {

    @GET("movie?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US")
    fun getListMovie(): Observable<MovieModel>

    @GET("tv?api_key=3d31335928249bca6a9f04d1d9d85d03&language=en-US")
    fun getListTv(): Observable<TvModel>
}